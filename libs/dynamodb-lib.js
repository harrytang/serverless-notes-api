/*
 * @Author: Harry Tang - harry@powerkernel.com
 * @Date: 2020-01-03 20:00:33
 * @Last Modified by: Harry Tang - harry@powerkernel.com
 * @Last Modified time: 2020-01-05 17:35:22
 */

import AWS from "aws-sdk";

export function call(action, params) {
  const dynamoDb = new AWS.DynamoDB.DocumentClient();
  return dynamoDb[action](params).promise();
}
